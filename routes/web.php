<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['namespace' => 'admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth'], function() {

	Route::get('/dashboard','DashboardController@index')->name('dashboard');

	/**
	* Admin Resources Module Routes
	*/
	Route::resources([
		'permissions'   => 'PermissionsController',
		'tasks'   => 'TasksController',
	]);

	Route::get('/timeline/{task_id}','TasksController@timeline')->name('tasks.timeline');
	Route::get('/tasklist/{cat_id}','TasksController@tasklist')->name('tasks.list');
	Route::get('/loadForm/{form_id}','TasksController@loadForm')->name('tasks.form');
	Route::post('/storeResponse','TasksController@storeResponse')->name('tasks.response');
});

Route::get('/pusher', function() {
    event(new App\Events\HelloPusherEvent('Hi there Pusher!'));
    return "Event has been sent!";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
