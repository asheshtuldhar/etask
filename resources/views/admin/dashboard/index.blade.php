@extends('template.layout')

@section('content')
<div class="main-content-inner">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home</a>
			</li>

			<li class="active">Tasks</li>
		</ul><!-- /.breadcrumb -->
	</div>
	<div class="page-content">

		<div class="page-header">
			<h1>
				Dashboard
			</h1>
		</div><!-- /.page-header -->
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-sm-12 infobox-container">
				<div id="messages">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

