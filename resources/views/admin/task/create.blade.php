@extends('template.layout')

@section('content')
<style type="text/css">
	.help-block {
		color: red;
	}
</style>
<div class="main-content-inner">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home</a>
			</li>

			<li class="active">Tasks</li>
		</ul><!-- /.breadcrumb -->
	</div>
	<div class="page-content">
		<div class="page-header">
			<div class="row">
				<div class="col-xs-10">
					<h1>
						Add Task
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							Assign task 
						</small>
					</h1>
				</div>
			</div>
		</div><!-- /.page-header -->
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			@if (Session::has('successmsg'))
				<div class="alert alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">
						<i class="ace-icon fa fa-times"></i>
					</button>

					<p>
						<strong>
							<i class="ace-icon fa fa-check"></i>
							Well done!
						</strong>
						{{ Session::get('successmsg') }}
					</p>
				</div>
			@endif
			<div class="col-xs-12">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Text Area</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form action="{{ $actionURL }}" method="POST" id="task-form">
									@csrf
									<div class="row">
										<div class="col-sm-3">
											<label for="form-field-8">Task Category</label>

											<select class="form-control" id="form-field-8" name="category_id">
												<option value="">Select Category</option>
												@foreach($categories as $category)
													<option value="{{ $category->id }}">{{ $category->name }}</option>
												@endforeach
											</select>
										</div>

										<div class="col-sm-3">
											<label for="form-field-8">Task Title</label>

											<input class="form-control" id="form-field-8" placeholder="Task Title" name="title">
										</div>

										<div class="col-sm-3">
											<label for="form-field-8">Task Assigned to</label>

											<select class="form-control" id="form-field-8" name="assigned_to">
												<option value="">Select User</option>
												@foreach($users as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<hr>

									<div class="row">
										<div class="col-sm-3">
											<label for="form-field-8">Task Description</label>

											<textarea class="form-control" id="form-field-8" placeholder="Description" name="description"></textarea>
										</div>
									</div><br>

									<div class="row">
										<div class="col-sm-12">
											<input type="submit" class="btn btn-success" value="Add">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.span -->
		</div><!-- /.row -->
		<!-- PAGE CONTENT ENDS -->
			
	</div>
</div>
@endsection