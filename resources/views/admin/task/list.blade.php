@extends('template.layout')

@section('content')
<div class="main-content-inner">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home</a>
			</li>

			<li class="active">Tasks</li>
		</ul><!-- /.breadcrumb -->
	</div>
	<div class="page-content">

		<div class="page-header">
			<div class="row">
				<div class="col-xs-10">
					<h1>
						Tasks
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							List of assigned Tasks
						</small>
					</h1>
				</div>
			</div>
		</div><!-- /.page-header -->
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				<table id="simple-table" class="table  table-bordered table-hover">
					<thead>
						<tr>
							<th>S.N</th>
							<th>Task No.</th>
							<th>Title</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tasks as $task)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $task->task_no }}</td>
								<td>{{ $task->title }}</td>
								<td>{{ $task->description }}</td>
								<td>
									<a href="#" class="btn btn-sm btn-primary">
										<span class="fa fa-edit"></span>
									</a>
									<a href="#" class="btn btn-sm btn-danger">
										<span class="fa fa-trash"></span>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div><!-- /.span -->
		</div><!-- /.row -->
		<!-- PAGE CONTENT ENDS -->
	</div>
</div>
@endsection