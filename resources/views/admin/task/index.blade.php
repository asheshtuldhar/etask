<?php use \App\Http\Controllers\admin\TasksController; ?>
@extends('template.layout')

@section('content')
<div class="main-content-inner">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home</a>
			</li>

			<li class="active">Tasks</li>
		</ul><!-- /.breadcrumb -->
	</div>
	<div class="page-content">

		<div class="page-header">
			<div class="row">
				<div class="col-xs-10">
					<h1>
						Tasks
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							List of assigned Tasks
						</small>
					</h1>
				</div>
				<div class="col-xs-2">
					<p class="pull-right">
						<a href="{{ route('admin.tasks.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add Task</a>
					</p>
				</div>
			</div>
		</div><!-- /.page-header -->
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				<table id="simple-table" class="table  table-bordered table-hover">
					<thead>
						<tr>
							<td style="width: 150px !important">Category Title</td>
							<td>Last Task</td>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category) 
							<tr>
								<td>
									<a href="{{ route('admin.tasks.list',$category->id) }}">
										{{ $category->name }} ({{ $category->task_cnt }})
									</a><br>
									<small>{{ $category->description }}</small>
								</td>
								<td>
									<a href="{{ route('admin.tasks.timeline',$category->task_id) }}">
										<b>{{ $category->task_no }}</b> : {{ !empty($category->title) ? $category->title : TasksController::gettitle($category->task_id) }}
									</a></p>
									<b>Created On: </b> {{ $category->created_at }}
								</td>
							</tr>
						@endforeach

						@if(empty($category))
							<tr>
								<td colspan="2">No records yet !!</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div><!-- /.span -->
		</div><!-- /.row -->
		<!-- PAGE CONTENT ENDS -->
	</div>
</div>
@endsection