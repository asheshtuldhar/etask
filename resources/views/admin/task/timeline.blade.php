<?php use \App\Http\Controllers\admin\TasksController; ?>
@extends('template.layout')
<?php //dd($threads); exit; ?>
@section('content')
	<div class="page-content">
		<div class="page-header">
			<h1>
				{{ $threads[0]->title }}
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					{{ $threads[0]->task_no }}
				</small>
			</h1>
		</div><!-- /.page-header -->

		<div id="timeline-1">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<div class="timeline-container">
						@foreach($threads as $thread)
							<div class="timeline-label">
								<span class="label label-primary arrowed-in-right label-lg">
									<b>{{ (date("Y-m-d") == date('Y-m-d',strtotime($thread->thread_created_at))) ? 'Today' : date('Y-m-d',strtotime($thread->thread_created_at)) }}</b>
								</span>
							</div>

							<div class="timeline-items">
								<div class="timeline-item clearfix">
									<div class="timeline-info">
										<img alt="Susan't Avatar" src="{{ asset('assets/images/avatars/avatar1.png') }}">
										<span class="label label-info label-sm">{{ date('H:i:s',strtotime($thread->thread_created_at)) }}</span>
									</div>

									<div class="widget-box transparent">
										<div class="widget-header widget-header-small">
											<h5 class="widget-title smaller">
												<a href="#" class="blue">{{ TasksController::getusername($thread->user_id) }}</a>
												<span class="grey">
													@if($thread->thread_type == 1) 
														added a task
													@elseif($thread->thread_type == 2)
														assigned task to <a href="#" class="blue">{{ TasksController::getusername($thread->assigned_to) }}</a>
													@elseif($thread->thread_type == 3)
														added a comment
													@elseif($thread->thread_type == 4)
														re-assigned task to <a href="#" class="blue">{{ TasksController::getusername($thread->assigned_to) }}</a>
													@endif
												</span>
											</h5>

											<span class="widget-toolbar no-border">
												<i class="ace-icon fa fa-clock-o bigger-110"></i>
												{{ date('H:i:s',strtotime($thread->thread_created_at)) }}
											</span>

											<span class="widget-toolbar">
												<a href="#" data-action="reload">
													<i class="ace-icon fa fa-refresh"></i>
												</a>

												<a href="#" data-action="collapse">
													<i class="ace-icon fa fa-chevron-up"></i>
												</a>
											</span>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												{{ $thread->description }}
												<div class="space-6"></div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- /.timeline-items -->
						@endforeach

						@if($threads[0]->status == 1)
							<div class="timeline-label">
								<span class="label label-primary arrowed-in-right label-lg">
									<b>{{ (date("Y-m-d") == date('Y-m-d',strtotime($threads[0]->task_updated_at))) ? 'Today' : date('Y-m-d',strtotime($threads[0]->task_updated_at)) }}</b>
								</span>
							</div>

							<div class="timeline-item clearfix">
								<div class="timeline-info">
									<i class="timeline-indicator ace-icon fa fa-check btn btn-success no-hover"></i>
								</div>

								<div class="widget-box transparent">
									<div class="widget-body">
										<div class="widget-main">
											Task has been marked as completed
											<div class="pull-right">
												<i class="ace-icon fa fa-clock-o bigger-110"></i>
												{{ date('H:i:s',strtotime($threads[0]->task_updated_at)) }}
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif

						@if($threads[0]->status == 2)
							<div class="timeline-label">
								<span class="label label-primary arrowed-in-right label-lg">
									<b>{{ (date("Y-m-d") == date('Y-m-d',strtotime($threads[0]->task_updated_at))) ? 'Today' : date('Y-m-d',strtotime($threads[0]->task_updated_at)) }}</b>
								</span>
							</div>

							<div class="timeline-item clearfix">
								<div class="timeline-info">
									<i class="timeline-indicator ace-icon fa fa-check btn btn-success no-hover"></i>
								</div>

								<div class="widget-box transparent">
									<div class="widget-body">
										<div class="widget-main">
											Task has been marked to re-do
											<div class="pull-right">
												<i class="ace-icon fa fa-clock-o bigger-110"></i>
												{{ date('H:i:s',strtotime($threads[0]->task_updated_at)) }}
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif
					</div><!-- /.timeline-container -->

					@if($threads[0]->status != 1)
						<p class="pull-right">
							<a href="javascript:;" data-href="{{ route('admin.tasks.form',3) }}" class="btn btn-white btn-success btn-bold bootbox-regular" data-title="Are you sure ?" data-formid="#responseform" data-responsetype = '3'>
								<i class="ace-icon fa fa-check bigger-120 green"></i>
								Mark as Done
							</a>

							<a href="javascript:;" data-title="Re-assign task" data-href="{{ route('admin.tasks.form',1) }}" class="btn btn-white btn-danger btn-bold bootbox-regular" data-formid="#responseform" data-responsetype = '1'>
								<i class="ace-icon fa fa-retweet bigger-120 red"></i>
								Re-assign
							</a>

							<a href="javascript:;" data-title="Add Response" data-href="{{ route('admin.tasks.form',2) }}" class="btn btn-white btn-success btn-bold bootbox-regular" data-formid="#responseform">
								<i class="ace-icon fa fa-reply bigger-120 blue" data-responsetype = '2'></i>
								Add response
							</a>
						</p>
					@else
						<p class="pull-right">
							<a href="javascript:;" data-href="{{ route('admin.tasks.form',4) }}" class="btn btn-white btn-danger btn-bold bootbox-regular" data-title="Are you sure?" data-formid='#responseform' data-responsetype = '4'>
								<i class="ace-icon fa fa-refresh bigger-120 red"></i>
								Redo Task
							</a>
						</p>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection