<form action="{{ $actionURL }}" method="POST" id="responseform">
	@csrf
	<input type="hidden" name="task_id" value="{{ $task_id }}" readonly="">
	{{-- for response type --}}
	<input type="hidden" name="response_type" value="{{ Request::segment(3) }}" readonly="">
	@if($form_id == 1) {{--  Reassign Form --}}
		<div class="row">
			<div class="col-sm-8">
				<label for="form-field-8">Task Assigned to</label>

				<select class="form-control" id="form-field-8" name="assigned_to">
					<option value="">Select User</option>
					@foreach($users as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<label for="form-field-8">Description</label>

				<textarea class="form-control" id="form-field-8" placeholder="Description" name="description"></textarea>
			</div>
		</div>
		</div>
	@elseif($form_id == 2) {{-- Add response form --}}
		<div class="row">
			<div class="col-sm-8">
				<label for="form-field-8">Description</label>

				<textarea class="form-control" id="form-field-8" placeholder="Description" name="description"></textarea>
			</div>
		</div>
	@elseif($form_id == 3) 
		<div class="row">
			<div class="col-sm-12">
				<h4><div class='alert alert-danger'>You are going to mark the task as done !!</div></h4>
			</div>
		</div>
	@elseif($form_id == 4) 
		<div class="row">
			<div class="col-sm-12">
				<h4><div class='alert alert-danger'>You are going to mark this task as re-do !!</div></h4>
			</div>
		</div>
	@endif
</form>