<!DOCTYPE html>
<html lang="en">
	@include('template.partials.htmlhead')

	<body class="no-skin">
		@include('template.partials.topbar')

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			@include('template.partials.sidebar')

			<div class="main-content">
				@yield('content')
			</div><!-- /.main-content -->

			<div class="footer">
				@include('template.partials.footer')
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		@include('template.partials.script')
		
	</body>
</html>
