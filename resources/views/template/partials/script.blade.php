<!-- basic scripts -->
<script src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/bootbox.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.gritter.min.js') }}"></script>

<!-- ace scripts -->
<!-- ace scripts -->
<script src="{{ asset('assets/js/ace-elements.min.js') }}"></script>
<script src="{{ asset('assets/js/ace.min.js') }}"></script>

<script src="{{ asset('assets/custom/js/main.js') }}"></script>
<script src="https://js.pusher.com/3.1/pusher.min.js"></script>

<script type="text/javascript">
	//instantiate a Pusher object with our Credential's key
	var pusher = new Pusher('f401448559c7e012871f', {
		cluster: 'ap2',
		encrypted: true
	});

	//Subscribe to the channel we specified in our Laravel Event
	var channel = pusher.subscribe('my-channel');

	//Bind a function to a Event (the full Laravel class)
	channel.bind('App\\Events\\HelloPusherEvent', addMessage);

	channel.bind('my-event', function(data) {
		alert(JSON.stringify(data));
	});

	function addMessage(data) {
		console.log(data.message);
		var item = `<li class="dropdown-content">
				<a href="#">
					<div class="clearfix">
						<span class="pull-left">
							<i class="btn btn-xs no-hover btn-pink fa fa-circle-o">
							</i>
							<b><span id="pushed_content"></span></b>
						</span>
					</div>
				</a>
			</li>`;
		var notification_cnt = parseInt($('.notification_cnt').text()) + parseInt(1);
		var listItem = $(item);
		listItem.find('span #pushed_content').replaceWith(data.message['task_no'] + ": " + data.message['title']);
		var user = "<?php echo Auth::user()->id ?>";

		if(user == data.message['user_id'] || user == data.message['assigned_to']) {
			$('.notification_cnt').html(function(i, val) { return +val+1 });
			$('#pusher_dropdown').prepend(listItem);
		}
	}
</script>
