
$(document).ready(function(){
	// Bootbox confirm
    $(document).off("click", ".bootbox-confirm").on("click", ".bootbox-confirm", function(e) {
        e.preventDefault();
        var location = $(this).data('href'); //data-href
        var msg = $(this).data('msg'); //data-msg

        bootbox.confirm(msg, function(result) {
            if(result) {
                window.location.replace(location);
                //console.log(location);
            }
        });
    });

    $(document).off("click", ".bootbox-regular").on("click", ".bootbox-regular", function(e) {
    	e.preventDefault();
    	var current_url = window.location.href.split('/').slice(3),
    		task_id = current_url[2];

    	var obj = $(this);
    	var url = obj.data('href'),
    	    title = obj.data('title');
    	$.ajax({
			type: 'get',
			dataType: 'json',
			url: url,
			data : {task_id},
    		headers: {
    		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		},
    		beforeSend : function(){
    			obj.button('loading');
    		},
    		success : function(resp){
    			if(resp.status == true){
	    			obj.button('reset');
					bootbox.dialog({
						message: resp.page,
						title: title,
						size: 'medium',
						buttons: {
						    "success" :
							 {
								"label" : "Save",
								"className" : "btn-sm btn-success",
								"callback": function() {
									return $.fn.submitForm(obj);
								}
							},
						}
					});
				}
			}
		})
	});

	$.fn.submitForm = function(obj) {
	    var formname = obj.data('formid');	
	    var responseType = obj.data('responsetype');
	    var action = $(formname).attr('action');

        $.ajax({
            url: action,
            type: 'post',
            data: new FormData($(formname)[0]),
            dataType: 'json',
            processData: false,
            contentType: false,
			beforeSend : function() {
				obj.button('loading');
			},            
            success: function(resp) {
                if (resp.status == true) {
                    $.gritter.add({
						title: 'Success!',
						text: 'Response Added Successfully',
						class_name: 'gritter-success'
					});
					window.location.reload();
                }else {
                	$.gritter.add({
						title: 'Error!',
						text: 'Failed to add response',
						class_name: 'gritter-error'
					});
                }
            }
        })
	};

	$('#task-form').validate({
		errorElement: 'div',
		errorClass: 'help-block',
		focusInvalid: false,
		ignore: "",
		rules: {
			category_id: {
				required: true,
			},
			title: {
				required: true
			},
			description: {
				required: true
			}
		},

		highlight: function (e) {
			$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
		},

		success: function (e) {
			$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
			$(e).remove();
		},
	});

	
})
