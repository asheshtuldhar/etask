<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Default Constructor
     */
    public function __construct()
    {
        /**
         * CRUD Lanaguage Message Replace Array
         */
        $this->crudLangRepArray = array('name' => $this->moduleName);
    }
}
