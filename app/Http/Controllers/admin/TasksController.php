<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

use App\Http\Controllers\Controller;
use DB;

use App\Repositories\Admin\Task\TaskRepository;
use App\Repositories\Admin\Thread\ThreadRepository;
use App\Events\HelloPusherEvent;

class TasksController extends Controller
{
    /**
     * Task Repository instance
     */
    private $taskRepository;
    private $threadRepository;

    /**
     * Default Constructor
     */
    public function __construct(TaskRepository $taskRepository,ThreadRepository $threadRepository)
    {
        /**
         * Set Current Module Name
         */
        $this->moduleName = __('crud.sys_tasks');
        /**
         * Current Repository Instances
         */
        $this->taskRepository = $taskRepository;
        $this->threadRepository = $threadRepository;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = DB::table('categories')
                ->select('categories.id','categories.name','categories.description',DB::raw('COUNT(DISTINCT tasks.id) AS task_cnt,MAX(tasks.id) AS task_id,MAX(tasks.task_no) AS task_no,MAX(threads.title) AS title,MAX(tasks.created_at) as created_at'))
                ->join('tasks', 'categories.id', '=', 'tasks.category_id')
                ->join('threads','threads.task_id', '=', 'tasks.id')->where('threads.id','>','tasks.id')
                ->where(function ($query) {
                    if(Auth::user()->id != 1) { //admin id : 1 
                        $query->where('threads.user_id',Auth::user()->id)
                        ->orWhereRaw('FIND_IN_SET(threads.assigned_to,'.Auth::user()->id.') <> 0');
                    }
                })
                ->groupBy('categories.id','categories.name','categories.description')->get();

        return view('admin.task.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users']          = DB::table('users')->get();
        $data['categories']     = DB::table('categories')->get();
        $data['actionURL']      = route('admin.tasks.store');

        return view('admin.task.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userID = Auth::user()->id;
        $input              = $request->all();
        $input['task_no']   = $this->generateRandomString().date('Y');

        $createTask     = $this->taskRepository->create($input);
        $task_id      = $createTask->getOriginal('id');
        
        $input['user_id']   = $userID; // for pusher
        $input['task_id'] = $task_id; // for pusher routing
        $assigned_to = $input['assigned_to'];
        $threadData = array(
            "task_id"       => $task_id,
            "thread_type"   => 1,
            "user_id"       => $userID,
            "title"         => $input['title'],
            "description"   => $input['description']
        );

        $createThread    = $this->threadRepository->create($threadData);
        if(!empty($assigned_to)) {
            $this->assignTask($assigned_to,$task_id,$userID);
        }
        if($createTask && $createThread) {
            event(new HelloPusherEvent($input));
            return redirect()->back()->with('successmsg', 'Task assigned successfully');
        } else {
            return redirect()->back()->with('errormsg', 'Failed to assign task');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display task timeline
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function timeline(Request $request,$task_id)
    {
        $this->viewData['threads'] = DB::table('threads')
                            ->select('tasks.id','tasks.task_no','tasks.status','threads.title','threads.description','threads.thread_type','threads.user_id','threads.assigned_to',DB::raw('tasks.created_at as task_created_at, tasks.updated_at as task_updated_at, threads.created_at as thread_created_at'))
                            ->join('tasks','tasks.id','=','threads.task_id')
                            ->orderBy('threads.created_at','asc')
                            ->where('task_id',$task_id)->get();
        return view('admin.task.timeline')->with($this->viewData);
    }

    /**
     * Display task list based on category
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tasklist(Request $request,$cat_id)
    {
        $this->viewData['tasks'] = DB::table('tasks')
                            ->join('threads','threads.task_id','=','tasks.id')
                            ->whereNotNull('threads.title')
                            ->where('category_id',$cat_id)->get();
        return view('admin.task.list')->with($this->viewData);
    }

    /**
     * Display task list based on category
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assignTask($assigned_to,$task_id,$userID) 
    {
        $threadData = array(
            "task_id"       => $task_id,
            "thread_type"   => 2,
            "user_id"       => $userID,
            "assigned_to"   => $assigned_to,
            "description"   => "Task Assigned"
        );

        $assignTask = $this->threadRepository->create($threadData);
        return $assignTask;
    }

    /**
     * Display task list based on category
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function loadForm(Request $request,$form_id)
    {
        $input = $request->all(); //for task_id
        $this->data['task_id'] = $input['task_id'];

        $this->page                 = "admin.task.form";
        $this->data['form_id']      = $form_id;
        $this->data['users']        = DB::table('users')->get();
        $this->data['actionURL']    = route('admin.tasks.response');
        if ($request->ajax()) {
            $html = view($this->page)->with($this->data)->render();
            return response()->json( array('status' => true, 'page'=>$html) );
        } 
    }

    /**
     * Display task list based on category
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function storeResponse(Request $request)
    {
        $input      = $request->all();
        $input['user_id'] = Auth::user()->id;
        $responseType = $input['response_type'];
        $task_id = $input['task_id'];

        if($responseType == 1) { //reassign task
            $input['thread_type'] = 4;
            $response   = $this->threadRepository->create($input);
        } else if($responseType == 2) { //add comment
                $input['thread_type'] = 3;
                $response   = $this->threadRepository->create($input);
        } else if($responseType == 3) { //mark as done
            $updateData = array(
                'status' => 1
            );
            $response = $this->taskRepository->update($updateData, $task_id);
        } else if($responseType == 4) { //redo task
            $updateData = array(
                'status' => 2
            );
            $response = $this->taskRepository->update($updateData, $task_id);
        }

        if($response) {
            return response()->json( array('status' => true, 'message' => 'Success') );
        }else {
            return response()->json( array('status' => false, 'message' => 'Error') );
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function getusername($id)
    {
        $user = DB::table('users')
            ->where('id',$id)->get()->first()->name;

        return $user;
    }

    public static function gettitle($id)
    {
        $title = DB::table('threads')
            ->where('task_id',$id)->whereNotNull('title')->get()->first()->title;

        return $title;
    }
}
