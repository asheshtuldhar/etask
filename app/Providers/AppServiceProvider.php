<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // Using Closure based composers...
        View::composer('template.partials.topbar', function($view)
        {
            $data['notifications'] = DB::table('tasks')
                ->select('tasks.id',DB::raw('MAX(tasks.task_no) as task_no,MAX(threads.title) AS title,MAX(threads.description) AS description'))
                ->leftJoin('threads','threads.task_id','=','tasks.id')
                ->where(function ($query) {
                    $query->where('threads.user_id',Auth::user()->id)
                        ->orWhereRaw('FIND_IN_SET(threads.assigned_to,'.Auth::user()->id.') <> 0');
                })
                ->where('tasks.status','<>',1)
                ->groupBy('tasks.id')->get();
            return $view->with($data);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
