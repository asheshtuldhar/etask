<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;


/*-----------------------------------------------------------------------------------------------------
 | All the repositories extend the BaseRepository Class. BaseRepository contains the basic functions
 | which are used by most of modules. It implements the BaseRepositoryInterface.
 ------------------------------------------------------------------------------------------------------*/
class BaseRepository implements BaseRepositoryInterface
{
    /**
     * model property on class instances
     */
    protected $model;

    /**
     * query instance of current model
     */
    protected $query;
    
    /**
     * Default Contructor of BaseRepository 
     * @param Model $model Repository Model Instance
     * @return void
     */
    public function __construct(Model $model)
    {
        /**
         * Set Model Instance
         */
        $this->model = $model;
    }

    /**
     *  Function returns all records from model
     */
    public function all()
    {
        return $this->model->get();
    }

    /**
     * Function that creates new record on database
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Function that updates record on database 
     */
    public function update(array $data, $id)
    {
        $record = $this->find($id);
        return $record->update($data);
    }

    /**
     * Function that deletes record form database
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Function that finds record from database
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Function that counts number of records
     */
    public function count()
    {
        return $this->model->count();
    }
    
    /**
     * Function that returns relationship with another model instance
     */
    public function with($relations = [])
    {
        return $this->model->with($relations);
    }
    
    /**
     * Function that relatinoships with other model instances
     */
    public function allWith($relations)
    {
        if (is_string($relations)) $relations = func_get_args();
        return $this->with($relations)->get();
    }

    /**
     * Function that find a record based on relationship
     */
    public function findWith($id, $relations=[])
    {
        return $this->with($relations)->findOrFail($id);
    }

    /**
     * Function that paginates database records
     */
    public function paginate($paginate)
    {
        return $this->model->paginate($paginate);
    }

    /**
     * Function that searches records based on conditions
     */
    public function where($name, $value)
    {
        return $this->model->where($name, $value);
    }
    
    /**
     * Function that return new query instance of current model
     */
    public function query()
    {
        return $this->model->newQuery();
    }

    /**
     * Get Current Model instance
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     *  Set the associated model instance
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
}
