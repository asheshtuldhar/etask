<?php
namespace App\Repositories\Admin\Permission;

use App\Repositories\BaseRepository;
use App\Permission;

/**
 * Permission Specific Repository
 */
class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    /**
     * Default Constructor
     */
    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }
}
