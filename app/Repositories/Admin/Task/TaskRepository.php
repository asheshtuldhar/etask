<?php
namespace App\Repositories\Admin\Task;

use App\Repositories\BaseRepository;
use App\Task;

/**
 * Task Specific Repository
 */
class TaskRepository extends BaseRepository implements TaskRepositoryInterface
{
    /**
     * Default Constructor
     */
    public function __construct(Task $Task)
    {
        $this->model = $Task;
    }
}
