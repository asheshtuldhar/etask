<?php
namespace App\Repositories\Admin\Thread;

use App\Repositories\BaseRepository;
use App\Thread;

/**
 * Thread Specific Repository
 */
class ThreadRepository extends BaseRepository implements ThreadRepositoryInterface
{
    /**
     * Default Constructor
     */
    public function __construct(Thread $thread)
    {
        $this->model = $thread;
    }
}
