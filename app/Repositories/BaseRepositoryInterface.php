<?php
namespace App\Repositories;

/*-----------------------------------------
 | Interface having core modules functiions
 ------------------------------------------*/
interface BaseRepositoryInterface
{
    /**
     *  Function returns all records from model
     */
	public function all();

    /**
     * Function that creates new record on database
     */
    public function create(array $data);

    /**
     * Function that updates record on database 
     */
    public function update(array $data, $id);

    /**
     * Function that deletes record form database
     */
    public function delete($id);

    /**
     * Function that finds record from database
     */
    public function find($id);

    /**
     * Function that counts number of records
     */
    public function count();
    
    /**
     * Function that returns relationship with another model instance
     */
    public function with($relations = []);
    
    /**
     * Function that relatinoships with other model instances
     */
    public function allWith($relations);

    /**
     * Function that find a record based on relationship
     */
    public function findWith($id, $relations=[]);

    /**
     * Function that paginates database records
     */
    public function paginate($paginate);

    /**
     * Function that searches records based on conditions
     */
    public function where($name, $value);
    
    /**
     * Function that return new query instance of current model
     */
    public function query();
}